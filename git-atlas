#!/bin/bash

# Author: K. Potamianos <karolos.potamianos@cern.ch
# Date: 2016-X-25

VERSION=01-00-01

baseDir=`pwd`

GIT_SPARSE_CHECKOUT_FILE=.git/info/sparse-checkout

#ATLASOFF_BASE=https://gitlab.cern.ch/atlas/athena.git
#ATLASOFF_FORK=${ATLASOFF_BASE/atlas/${GIT_USER}}
#ATLAS_GIT_PROJECT=`basename ${ATLASOFF_BASE/.git}`

usage () {
  ${ECHO} "Usage: git atlas init-config [CERN login] [--apply]"
  ${ECHO} "   or: git atlas init-workdir [-p pkgList [--]] [-b branch] [-g group] baseRepo [localPath]"
  ${ECHO} "       pkgList can be a file containing package paths, or a list of packages"
  ${ECHO} "       To save disk space, baseRepo may be a local workdir of the same project"
  ${ECHO} "Options:"
  ${ECHO} "-b, --branch  \tBranch you want to check out (for init-workdir)"
  ${ECHO} "-g, --group   \tGitLab group to use instead of 'atlas'"
  ${ECHO} "-p            \tPackages for sparse-checkout, provided via either"
  ${ECHO} "              \tfile, or list terminated by option, -- or repo source (baseRepo)"
  ${ECHO} ""
  ${ECHO} "The following flavors are availble from within a clone/checkout:"
  ${ECHO} "   or: git atlas list-pkg [--all]"
  ${ECHO} "   or: git atlas addpkg|rmpkg PackageName"
  ${ECHO} ""
  ${ECHO} "Global options:"
  ${ECHO} "-h, --help    \tThis help"
  ${ECHO} "-v, --verbose \tVerbose output (level 2)"
  ${ECHO} "-V, --version \tDisplay version"
  ${ECHO} "-q, --quiet   \tQuiet output (verbosity level 0)"
  ${ECHO} "-a, --all     \tSet 'all' flag for the commands who need it"
  ${ECHO} "-n, --dry-run \tDry run: do not execute any command"
  exit 0
}

die () {
  ${ECHO} "${ECHO} \"${redColor}$@${defaultColor}\"" | bash
  exit 128
}

GIT_ATLAS_COLOR=0
GIT_COLOR_CFG_UI=`git config color.ui`
GIT_COLOR_CFG_ATLAS=`git config color.atlas`
if [ "${GIT_COLOR_CFG_ATLAS}" != "" ] ; then
  [ "${GIT_COLOR_CFG_ATLAS}" == "true" ] && GIT_ATLAS_COLOR=1
  [ "${GIT_COLOR_CFG_ATLAS}" == "always" ] && GIT_ATLAS_COLOR=1
  [[ -t 1 && "${GIT_COLOR_CFG_ATLAS}" == "auto" ]] && GIT_ATLAS_COLOR=1
else
  [ "${GIT_COLOR_CFG_UI}" == "always" ] && GIT_ATLAS_COLOR=1
  [[ -t 1 && "${GIT_COLOR_CFG_UI}" == "auto" ]] && GIT_ATLAS_COLOR=1
fi

if [ ${GIT_ATLAS_COLOR} -eq 1 ] ; then
  redColor="\e[31m"
  greenColor="\e[32m"
  blueColor="\e[34m"
  yellowColor="\e[33m"
  defaultColor="\e[39m"
fi

thisFunc="git-atlas"

git-atlas-logger () {
  mType=${1}
  vLevel=${2}
  logMsg="${3}"
  textColor=${greenColor}
  [ x${mType} == xwarn ] && textColor=${yellowColor}
  [ x${mType} == xerror ] && textColor=${redColor}
  [ ${GA_VERBOSITY} -ge ${vLevel} ] && ${ECHO} "${ECHO} \"\n${textColor}# [${thisFunc}] ${logMsg}${defaultColor}\"" | bash
  [ x${mType} == xerror ] && exit 128
}

is-dry-run () {
  [ ${GA_DRY_RUN} -ne 0 ]
}

run-git () {
  if is-dry-run ; then
    ${ECHO} "${ECHO} \"${blueColor}[dry-run]: $@${defaultColor}\"" | bash
  else
    [ ${GA_VERBOSITY} -ge 2 ] && ${ECHO} "${ECHO} [running]: git $@\n" | bash
    git "$@"
  fi
  gitExitCode=$?
  [ ${gitExitCode} -ne 0 ] && git-atlas-logger error 1 "There was a problem running 'git $@'"
  return ${gitExitCode}
}

# Processing the arguments

IS_INIT_CMD=0
GA_VERBOSITY=1
GA_DRY_RUN=0
GA_ALL=0
GA_BRANCH=master
GA_GROUP="atlas"
GA_USAGE=0
GA_PKGS=()
# 0: not started ; 1: ongoing ; 2: done
PKGPROC=0

args=()
while [[ $# -gt 0 ]]
do
  [ "${1/init-}" != "${1}" ] && IS_INIT_CMD=1
  # Set package argument to 'done' when encountering any option
  case "${1}" in
    -*) [ x${PKGPROC} == x1 ] && PKGPROC=2 ;;
  esac
  case "${1}" in
    -h) shift ; GA_USAGE=1 ;; # Don't add help: that's for the man page
    -b|--branch) shift ; GA_BRANCH="${1}" ;;
    -g|--group) shift ; GA_GROUP="${1}" ;;
    -n|--dry-run) GA_DRY_RUN=1 ;;
    -q|--quiet) GA_VERBOSITY=0 ;;
    -v|--verbose) GA_VERBOSITY=2 ;;
    -V|--version) echo Git-Atlas version ${VERSION} && exit ;;
    -p) shift ; PKGPROC=1 ; GA_PKGS+=("${1}") ;;
    --) PKGPROC=2 ;;
    -vv) GA_VERBOSITY=3 ;;
    -vvv) GA_VERBOSITY=4 ;;
    -a|--all) GA_ALL=1 ;;
    --apply) APPLY=1 ;;
    *) if [ x${PKGPROC} == x1 ] ; then
         # Argument has '/': it's not a package
         [[ "${1/\/}" != "${1}" || -d ${1} ]] && PKGPROC=2
         [ x${PKGPROC} == x1 ] && GA_PKGS+=("${1}") || args+=("${1}")
       else
         args+=("${1}")
       fi ;;
  esac
  shift
done

set -- "${args[@]}"

ECHO="echo -e"
GIT="run-git"


git_config="git config"
[ -z "${GIT_CONFIG}" ] && git_config+=" --global"

# Add configurable user
GIT_USER=`${git_config} atlas.user 2>/dev/null`
[ "$GIT_USER" == "" ] && GIT_USER=${USER}

if [[ "${args}" != "" && x${GA_USAGE} == x0 && x${IS_INIT_CMD} == x0 ]] ; then
  gitTopLevelDir=`${GIT} rev-parse --show-toplevel`
  [ $? -ne 0 ] && git-atlas-logger error 1 "Could not determine the root of the git working copy. Make sure you're in one."
fi

get_repo_url () {
  repo_url=`git config --get remote.origin.url`
  repo_url2=`get_url_from_insteadOf ${repo_url}`
  [ -z "${repo_url2}" ] && echo ${repo_url} || echo ${repo_url2}
}

get_atlas_repo_url () {
  atlas_repo_url=`git config --get remote.upstream.url`
  atlas_repo_url2=`get_url_from_insteadOf ${atlas_repo_url}`
  [ -z "${atlas_repo_url2}" ] && echo ${atlas_repo_url} || echo ${atlas_repo_url2}
}

get_url_from_insteadOf () {
  insteadOf=${1}
  git config --get-regexp "^url.*" ${insteadOf} | sed 's/url\.\(.*\).insteadof.*/\1/' | head -1
}

get_project_name () {
  basename `get_repo_url` | sed 's/.git$//'
}

# This function sets up basic git config items used for ATLAS

init-config () {
  thisFunc="git-atlas init-config"

  cernLogin=${1:-${USER}}
  [ -z ${1} ] && git-atlas-logger warn 1 "No CERN login provided: assuming ${USER}"

  git-atlas-logger info 1 "Fetching ATLAS configuration items"

  : > /tmp/config-cmds

  # Todo: cleanup implementation
  if [ "`${git_config} atlas.user`" == "" ] ; then
    git-atlas-logger info 2 "Setting user to ${cernLogin}"
    ${ECHO} "${git_config} atlas.user ${cernLogin}" >> /tmp/config-cmds
  fi

  if [ "`${git_config} url.\"https://gitlab.cern.ch/atlas/athena\".insteadOf`" == "" ] ; then
    git-atlas-logger info 2 "Creating aliases for ATLAS GitLab repositories"
    ${ECHO} "${git_config} url.\"https://gitlab.cern.ch/atlas/athena\".insteadOf athena:" >> /tmp/config-cmds
  fi

  [ `git config --get-regexp '^user\.(name|email)$' | wc -l` -eq 2 ] && has_user_config=1 || has_user_config=0
  if [[ ${has_user_config} -eq 1 && `git config --get user.email | grep -c cern.ch` -ne 1 ]] ; then
    git-atlas-logger warn 1 "Your (global) git user email is not in the .cern.ch domain.
    Assuming you did that on purpose. But I'll use your CERN email when checkign out an ATLAS repository"
  fi
  # Todo: handle the case where the system user name doesn't match the git user name
  if [[ ${has_user_config} -eq 0 &&  "`which ldapsearch 2>/dev/null`" != "" ]] ; then
    git-atlas-logger info 2 "Querying LDAP for your user name and email address"
    tmpFile=/tmp/ldapSearch-$(date +%s).out
    # Todo: andle case were LDAP user is not found
    if [[ `hostname` =~ .cern.ch ]] ; then
      ldapsearch -x -h xldap.cern.ch -b "ou=users,ou=organic units,dc=cern,dc=ch" -s sub "(&(cn=${cernLogin})(objectClass=organizationalPerson))" > ${tmpFile}
    else
      ldapsearch -D "cn=${cernLogin},ou=users,o=cern,c=ch" -W -x -H ldaps://ldap.cern.ch:636 -b "o=cern,c=ch" -s sub "(&(uid=${cernLogin}))" > ${tmpFile}
    fi
    [ $? -ne 0 ] && ${ECHO} "ERROR: cannot connect to LDAP to obtain CERN email address" > /dev/stderr
    grep "^displayName:\|^mail:" ${tmpFile} | \
      sed -e "s/displayName: /${git_config} user.name \"/" -e "s/$/\"/" -e "s/mail: /${git_config} user.email \"/" >> /tmp/config-cmds
    rm ${tmpFile}
  fi

  if [ `grep -c . /tmp/config-cmds` -ne 0 ] ; then
    if [ x${APPLY} == x1 ] ; then
      git-atlas-logger info 1 "Applying configuration settings"
      bash -x /tmp/config-cmds
    else
      git-atlas-logger warn 1 "The following commands will be executed when setting the --apply option:"
      cat /tmp/config-cmds
    fi
  else
    git-atlas-logger info 1 "You're all set: nothing to add to git config"
  fi

  rm /tmp/config-cmds

}

get_user_fork () {
  # Assuming the project names are the same
  atlasRepo="${1}"
  echo ${atlasRepo/\/atlas\//\/${GIT_USER}\/}
}

get_group_repo () {
  srcRepo="${1}"
  userName=`dirname ${srcRepo}`
  userName=`basename ${userName%/}`
  echo ${srcRepo/\/${userName}\//\/${GA_GROUP}\/}
}

init-workdir () {
  thisFunc="git-atlas init-workdir"
  [ -z ${1} ] && usage
  srcRepo="${1}"
  destDir="${2}"
  pkgList="${GA_PKGS[@]}"

  atlasBranch="${GA_BRANCH}"
  groupRepo=`get_group_repo ${srcRepo}`

  # Expand srcRepo
  [ "${srcRepo: -1}" == ":" ] && srcRepo=`get_url_from_insteadOf ${srcRepo}`
  [[ "${destDir}" == "" ]] && destDir=`basename ${srcRepo%.git}`

  if [ `echo "${srcRepo}" | grep -c 'https://\|ssh://'` -ne 1 ] ; then
    if [[ ! -e "${srcRepo}/config" && ! -e "${srcRepo}/.git/config" ]] ; then
      git-atlas-logger error 1 "ERROR: ${srcRepo} doesn't seem to be a git repository"
    fi
  fi

  if [ "${pkgList}" == "" ] ; then
    pkgInfo="* you didn't request any package to be checked out"
  elif [ "${pkgList}" != "" ] ; then
    if [[ -e "${baseDir}/${pkgList}" || -L "${baseDir}/${pkgList}" ]] ; then
      pkgInfo="* cheking out the items listed in ${pkgList}:\n `cat ${pkgList} | sed 's/^/	/'`"
    else
      pkgInfo="* checking out the following packages: ${pkgList}"
    fi
  fi

  if [ -d ${srcRepo}/.git ] ; then
    pushd ${srcRepo} >/dev/null
    atlasRepoUrl=`get_atlas_repo_url`
    userFork=`get_user_fork ${atlasRepoUrl}`
    popd >/dev/null
    if [ -L ${srcRepo}/.git/objects ] ; then
      baseRepo=`readlink ${srcRepo}/.git/objects` && baseRepo=${baseRepo/.git*}
      git-atlas-logger warn 1 "Detected that ${srcRepo} is not a base checkout (i.e. .git is already symlinked)
      Using the source of the link (in case you delete the working copy): ${baseRepo}"
      srcRepo=${baseRepo}
    fi
    git-atlas-logger info 1 "Creating new working directory at ${destDir}:
    * switching to branch ${atlasBranch} of your fork ${userFork}
    * BUT using the .git local repository from ${srcRepo}
    * using the `basename ${srcRepo%.git}` project at ${atlasRepoUrl} as the 'upstream'
    ${pkgInfo}"
    if [ -d ${destDir} ] ; then
      destDirAbsPath="`cd ${destDir} && pwd`"
      git-atlas-logger error 1 "There's already a repository at ${destDirAbsPath}"
    fi
    ${GIT} new-workdir --sparse --no-sparse-edit ${srcRepo} ${destDir} ${atlasBranch}
    cd ${destDir}
    destDirAbsPath=`pwd`
  else
    userFork="`get_user_fork ${srcRepo}`"
    mkdir -p ${destDir} && destDirAbsPath="`cd ${destDir} && pwd`"
    git-atlas-logger info 1 "Creating new working directory at ${destDir}:
    * switching to branch ${atlasBranch} of your fork ${userFork}
    * using ${groupRepo} as the 'upstream'
    ${pkgInfo}"
    [ -d ${destDirAbsPath}/.git ] && git-atlas-logger error 1 "There's already a repository at ${destDirAbsPath}"
    atlasRepo=${srcRepo}

    # Using 'old' commands on purpose, for maximum compatibility with git versions
    # Otherwise could use git clone --no-checkout --config core.sparsecheckout=true ...
    cd ${destDir}
    ${GIT} init
    ${GIT} remote add origin ${userFork}

    ${GIT} remote add upstream ${groupRepo}

    ${GIT} config --local core.sparsecheckout true

    ${GIT} fetch origin
    ${GIT} fetch upstream
fi

  is-dry-run || : > ${GIT_SPARSE_CHECKOUT_FILE}

  WorkDir=AthenaWorkDir
  WorkDir=WorkDir

  echo "Projects/${WorkDir}" >> ${GIT_SPARSE_CHECKOUT_FILE}
  git-atlas-logger warn 1 "Adding one package needed by CMake: ${WorkDir}"

  if [ "`${GIT} branch`" == "" ] ; then
    ${GIT} checkout --no-track -b ${atlasBranch} upstream/${atlasBranch}
  else
    ${GIT} checkout
  fi

  [ `GA_ALL=1 list-pkg ${WorkDir} | grep -c Projects/${WorkDir}` -eq 0 ] &&
    git-atlas-logger error 1 "Branch ${atlasBranch} not supported: missing Projects/${WorkDir}/CMakeLists.txt\n \
    Please use a branch that contains it using the -b BRANCH option, or \\\`cd ${destDir}; git checkout BRANCH\\\`\n \
    Currently supported options for BRANCH are:\n`ls-branch-with-pkg Projects/${WorkDir} | sed 's/^/       /'`"

  DO_CHECKOUT=0
  if [ "${pkgList}" == "" ] ; then
    git-atlas-logger info 1 "You haven't specified a list of packages:
    * use git atlas addpkg from your checkout to add some"
  elif [[ -f "${baseDir}/${pkgList}" ]] ; then
    pkgListAbsPath=${baseDir}/${pkgList}
    git-atlas-logger info 2 "File '${pkgListAbsPath}' will be used to populate .git/info/sparse-checkout"
    cat "${pkgListAbsPath}" > ${GIT_SPARSE_CHECKOUT_FILE}
    DO_CHECKOUT=1
  else
    # Upstream only needed for the initial checkout
    export GIT_LS_FILES_BRANCH=upstream/${atlasBranch}
    for pkg in ${pkgList} ; do
      manage-pkg addpkg ${pkg}
    done
    DO_CHECKOUT=1
    unset GIT_LS_FILES_BRANCH
  fi
  [ ${DO_CHECKOUT} -eq 1 ] && ${GIT} checkout

  ${ECHO} ""
  ${ECHO} "Initial checkout at ${destDirAbsPath}"
  ${ECHO} "Use git atlas addpkg to add a package."
}

manage-pkg () {
  thisFunc="git-atlas manage-pkg"
  pkgAction=${1}
  pkgName=${2}
  git-atlas-logger info 1 "Called with action '${pkgAction}' and package name '${pkgName}'"

  if [[ `git diff --shortstat 2> /dev/null | tail -n1` != "" ]] ; then
    git-atlas-logger error 1 "${pkgAction} is not supported with unclean working copies.
      Please cleanup your working copy by either:
      * committing your changes, OR
      * stashing them away using \\\`git stash\\\` (and \\\`git stash pop\\\` after running ${pkgAction})"
  fi
  if [ "${pkgAction}" == "rmpkg" ] ; then
    if [[ "${pkgName}" == "${WorkDir}" ]] ; then
      git-atlas-logger error 1 "You tried to remove some packages needed by CMake: ${WorkDir}
        This is not possible for the time being, so this command was ignored.
        Note: this requirement will be removed in the future."
    fi
  fi
  ${GIT} sparse-checkout-atlas ${pkgAction} ${pkgName}
  [[ $? -eq 0 && "${GIT_LS_FILES_BRANCH}" == "" && "`${GIT} branch`" != "" ]] && ${GIT} checkout
  ${ECHO} ""
}

list-pkg () {
  thisFunc="git-atlas list-pkg"
  regexp="${1}"
  if [ ${GA_ALL} -eq 1 ] ; then
    git-atlas-logger info 1 "Listing all known ATLAS packages (i.e. paths with CMakeLists.txt) matching '${regexp}'"
    git ls-tree --name-only -r HEAD --full-tree | grep CMakeLists.txt | sed 's@/CMakeLists.txt@@' | grep -i "${regexp}"
  else
    ${GIT} sparse-checkout-atlas list
  fi
}

ls-branch-with-pkg () {
pkgName="${1}"
for b in $(git branch -a) ; do
  git ls-tree -r --name-only $b | grep --quiet "${pkgName}/CMakeLists.txt" && echo $b
done | sed -e "s@remotes/\(upstream\|origin\)/@@" | sort -u
}

action=${1}
shift
args="${@}"

case "${action}" in
init-config) init-config ${args} ;;
init-workdir) init-workdir ${args} ;;
addpkg|rmpkg) for a in ${args} ; do manage-pkg ${action} ${a} ; done ;;
addpath|rmpath) for a in ${args} ; do manage-pkg ${action} ${a} ; done ;;
lspkg|listpkg|list-pkg) list-pkg ${args} ;;
*) usage ;;
esac



if [ ! -z "$ATLAS_LOCAL_GIT_PATH" ]; then
  basePath="$ATLAS_LOCAL_GIT_PATH"
else
  basePath=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/git/GIT_VERSION_ARCH/
fi

export PATH=${basePath}/bin:${PATH}
export MANPATH=${basePath}/docs/man:${MANPATH}
export GIT_EXEC_PATH=${basePath}/libexec/git-core/
export GIT_TEMPLATE_DIR=${basePath}/share/git-core/templates

#Introduction
The Dockerfile will generate a git installation (with the git-atlas tools) ready for installation on CVMFS. 
git is not fully relocatable so the Docker container makes a fake /cvmfs directory structure then compiles git from source.

#Basic example:
```
docker build . -t build_tag_x86
```


#ARM build
```
docker build . --build-arg IMAGE=arm64v8/centos:7 --build-arg ARCH=aarch64 --build-arg GIT_LFS_ARCH=arm64 --build-arg OS=Linux -t build_tag_aarch64
```

To extract the tarball, run the resulting container interactively and copy the tarball to the mounted directory
```
docker run -it --mount src="$(pwd)",target=/test_container,type=bind build_tag_x86
#within container
cp git-2.26.2-x86_64-centos7.tgz /test_container
```
